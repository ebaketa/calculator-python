# Calculator

## Calculator Python GTK GUI Application

**Hardware:**
- Linux PC

**Software:**
- Debian Linux OS (Or any other distribution)
- Python 3
- PyGObject

##  Creation of virtual environments
- python3 -m venv .venv

**Installing system dependencies:**
- apt install libgirepository1.0-dev

**Installing modules with pip3:**
- . .venv/bin/activate
- pip3 install PyGObject

## Starting project:

python3 ./calculator.py
