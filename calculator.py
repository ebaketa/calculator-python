# *****************************************************************************
# Program:      Calculator Gtk GUI
# Author:       Elvis Baketa
#               Python 3
#               PyGObject
# *****************************************************************************

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Pango

class mainWindow():
    def __init__(self):
        # application window
        gladeFile = "calculator.glade"
        self.builder = Gtk.Builder()
        self.builder.add_from_file(gladeFile)
        self.builder.connect_signals(self)
        self.window = self.builder.get_object("mainWindow")
        # set window position
        self.window.set_position(Gtk.WindowPosition.CENTER)
        # set windows title
        self.window.set_title("Calculator")
        self.window.connect("key-press-event",self.on_key_press_event)
        self.window.connect("destroy", Gtk.main_quit)
        # self.window.connect("delete-event", self.on_delete)
        self.window.connect("realize", self.on_realize)

        # global variables
        self.display = ''
        self.decimalPoint = False
        self.firstDigitNull = False
        self.number = []
        self.arithmeticOperation = ''
        self.addOperation = False
        self.subtractOperation = False
        self.multiplyOperation = False
        self.divideOperation = False
        self.isfloat = False

        #
        self.entDisplay = self.builder.get_object("entDisplay")
        self.entDisplay.set_alignment(1)
        self.btnN0 = self.builder.get_object("btnN0")
        self.btnN1 = self.builder.get_object("btnN1")
        self.btnN2 = self.builder.get_object("btnN2")
        self.btnN3 = self.builder.get_object("btnN3")
        self.btnN4 = self.builder.get_object("btnN4")
        self.btnN5 = self.builder.get_object("btnN5")
        self.btnN6 = self.builder.get_object("btnN6")
        self.btnN7 = self.builder.get_object("btnN7")
        self.btnN8 = self.builder.get_object("btnN8")
        self.btnN9 = self.builder.get_object("btnN9")
        self.btnDot = self.builder.get_object("btnDot")
        self.btnC = self.builder.get_object("btnC")
        self.btnAdd = self.builder.get_object("btnAdd")
        self.btnSubtract = self.builder.get_object("btnSubtract")
        self.btnMultipy = self.builder.get_object("btnMultipy")
        self.btnDivide = self.builder.get_object("btnDivide")
        self.btnEqually = self.builder.get_object("btnEqually")

        # 
        self.btnN0.connect("clicked", self.clicked_Numeric, 0)
        self.btnN1.connect("clicked", self.clicked_Numeric, 1)
        self.btnN2.connect("clicked", self.clicked_Numeric, 2)
        self.btnN3.connect("clicked", self.clicked_Numeric, 3)
        self.btnN4.connect("clicked", self.clicked_Numeric, 4)
        self.btnN5.connect("clicked", self.clicked_Numeric, 5)
        self.btnN6.connect("clicked", self.clicked_Numeric, 6)
        self.btnN7.connect("clicked", self.clicked_Numeric, 7)
        self.btnN8.connect("clicked", self.clicked_Numeric, 8)
        self.btnN9.connect("clicked", self.clicked_Numeric, 9)
        self.btnDot.connect("clicked", self.clicked_Numeric, '.')
        self.btnC.connect("clicked", self.clicked_C)
        self.btnAdd.connect("clicked", self.clicked_addNumbers)
        self.btnSubtract.connect("clicked", self.clicked_subtractNumbers)
        self.btnMultipy.connect("clicked", self.clicked_multiplyNumbers)
        self.btnDivide.connect("clicked", self.clicked_divideNumbers)
        self.btnEqually.connect("clicked", self.clicked_equallyNumbers)

    # 
    def on_realize(self, data=None):
        self.display = '0'
        self.decimalPoint = False
        self.firstDigitNull = False
        self.entDisplay.set_text(self.display)

    # function that resolve key press events
    def on_key_press_event(self, widget, event):
        if(event.keyval == Gdk.KEY_KP_0 or event.keyval == Gdk.KEY_0):
            self.clicked_Numeric("clicked", 0)
        elif(event.keyval == Gdk.KEY_KP_1 or event.keyval == Gdk.KEY_1):
            self.clicked_Numeric("clicked", 1)
        elif(event.keyval == Gdk.KEY_KP_2 or event.keyval == Gdk.KEY_2):
            self.clicked_Numeric("clicked", 2)
        elif(event.keyval == Gdk.KEY_KP_3 or event.keyval == Gdk.KEY_3):
            self.clicked_Numeric("clicked", 3)
        elif(event.keyval == Gdk.KEY_KP_4 or event.keyval == Gdk.KEY_4):
            self.clicked_Numeric("clicked", 4)
        elif(event.keyval == Gdk.KEY_KP_5 or event.keyval == Gdk.KEY_5):
            self.clicked_Numeric("clicked", 5)
        elif(event.keyval == Gdk.KEY_KP_6 or event.keyval == Gdk.KEY_6):
            self.clicked_Numeric("clicked", 6)
        elif(event.keyval == Gdk.KEY_KP_7 or event.keyval == Gdk.KEY_7):
            self.clicked_Numeric("clicked", 7)
        elif(event.keyval == Gdk.KEY_KP_8 or event.keyval == Gdk.KEY_8):
            self.clicked_Numeric("clicked", 8)
        elif(event.keyval == Gdk.KEY_KP_9 or event.keyval == Gdk.KEY_9):
            self.clicked_Numeric("clicked", 9)
        elif(event.keyval == Gdk.KEY_KP_Separator or event.keyval == Gdk.KEY_period):
            self.clicked_Numeric("clicked", '.')
        elif(event.keyval == Gdk.KEY_Escape or event.keyval == Gdk.KEY_BackSpace):
            self.clicked_C("clicked")
        elif(event.keyval == Gdk.KEY_KP_Add or event.keyval == Gdk.KEY_plus):
            self.clicked_addNumbers("clicked")
        elif(event.keyval == Gdk.KEY_KP_Subtract or event.keyval == Gdk.KEY_minus):
            self.clicked_subtractNumbers("clicked")
        elif(event.keyval == Gdk.KEY_KP_Multiply):
            self.clicked_multiplyNumbers("clicked")
        elif(event.keyval == Gdk.KEY_KP_Divide):
            self.clicked_divideNumbers("clicked")
        elif(event.keyval == Gdk.KEY_KP_Enter or event.keyval == Gdk.KEY_Return):
            self.clicked_equallyNumbers("clicked")

        print("Key press on widget: ", widget)
        print("          Modifiers: ", event.state)
        print("      Key val, name: ", event.keyval, Gdk.keyval_name(event.keyval))

    # 
    def clicked_Numeric(self, button, data):
        data = str(data)
        if(data.isdigit() and data == '0'):
            if(data == '0' and self.firstDigitNull == False):
                self.firstDigitNull = True
                self.decimalPoint = True
                self.display = self.display + '.'
                self.entDisplay.set_text(self.display)
            elif(data != '0' and self.firstDigitNull == True):
                self.display = self.display + str(data)
                self.entDisplay.set_text(self.display)
            elif(data == '0' and self.firstDigitNull == True):
                self.display = self.display + str(data)
                self.entDisplay.set_text(self.display)
        elif(data.isdigit() and data != '0'):
            if(self.firstDigitNull == False):
                self.firstDigitNull = True
                self.display = ''
                self.display = self.display + str(data)
                self.entDisplay.set_text(self.display)
            else:
                self.display = self.display + str(data)
                self.entDisplay.set_text(self.display)
        elif(data == '.' and self.decimalPoint == False):
            self.decimalPoint = True
            self.display = self.display + str(data)
            self.entDisplay.set_text(self.display)

    # 
    def clicked_C(self, button):
        self.display = '0'
        self.decimalPoint = False
        self.firstDigitNull = False
        self.addOperation = False
        self.subtractOperation = False
        self.multiplyOperation = False
        self.divideOperation = False
        self.entDisplay.set_text(self.display)

    # add
    def clicked_addNumbers(self, button):
        self.addOperation = True
        self.number.append(self.display)
        self.arithmeticOperation = '+'
        self.display = '0'
        self.decimalPoint = False
        self.firstDigitNull = False
        
    # subtract
    def clicked_subtractNumbers(self, button):
        self.subtractOperation = True
        self.number.append(self.display)
        self.arithmeticOperation = '-'
        self.display = '0'
        self.decimalPoint = False
        self.firstDigitNull = False

    # multiply
    def clicked_multiplyNumbers(self, button):
        self.multiplyOperation = True
        self.number.append(self.display)
        self.arithmeticOperation = '+'
        self.display = '0'
        self.decimalPoint = False
        self.firstDigitNull = False

    # divide
    def clicked_divideNumbers(self, button):
        self.divideOperation = True
        self.number.append(self.display)
        self.arithmeticOperation = '+'
        self.display = '0'
        self.decimalPoint = False
        self.firstDigitNull = False

    # equally
    def clicked_equallyNumbers(self, button):
        self.number.append(self.display)
        if(self.number[0].find(".") == 1):
            self.isfloat = True
        elif(self.number[1].find(".") == 1):
            self.isfloat = True

        if(self.addOperation == True):
            if(self.isfloat == True):
                result = float(self.number[0]) + float(self.number[1])
            else:
                result = int(self.number[0]) + int(self.number[1])
        elif(self.subtractOperation == True):
            if(self.isfloat == True):
                result = float(self.number[0]) - float(self.number[1])
            else:
                result = int(self.number[0]) - int(self.number[1])
        if(self.multiplyOperation == True):
            if(self.isfloat == True):
                result = float(self.number[0]) * float(self.number[1])
            else:
                result = int(self.number[0]) * int(self.number[1])
        elif(self.divideOperation == True):
            if(self.isfloat == True):
                result = float(self.number[0]) / float(self.number[1])
            else:
                result_rest = int(self.number[0]) % int(self.number[1])
                if(result_rest != 0):
                    result = float(self.number[0]) / float(self.number[1])
                else:
                    result = int(self.number[0]) / int(self.number[1])

        self.display = str(result)
        self.entDisplay.set_text(self.display)
        self.number = []
        self.addOperation = False
        self.subtractOperation = False
        self.multiplyOperation = False
        self.divideOperation = False
        self.isfloat = False

if __name__ == '__main__':
    main = mainWindow()
    # show application window
    main.window.show()
    Gtk.main()
